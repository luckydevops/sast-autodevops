password99= "password" 

import python
from CallNode call
where call = Value::named("yaml.load").getACall()
select call.getNode(), "yaml.load function is unsafe when loading data from untrusted sources. Use yaml.safe_load instead."
